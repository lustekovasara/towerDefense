#include "map.hpp"

using namespace std;

bool Map::loadMap( int chosenMap ) {

    string mapLine;

    ifstream mapFile;
    mapFile.open( "src/files/map" + to_string( chosenMap ) + ".txt");
    if( !mapFile. is_open() ){

        return false;

    };
    if( !mapFile.good() ){

        mapFile.close();
        return false;

    }

    map.clear();

    int i = 0, j;
    while( getline( mapFile, mapLine ) ){

        if( mapLine.empty( ) ){

            break;

        }
        map.push_back( vector< shared_ptr< Field > > ( ) );

        int size = ( int ) mapLine.size();
        for( j = 0 ; j < size ; j++ ){

            shared_ptr<Field> field = make_shared<Field> ( Field() );

            field.get()->setCharacter( mapLine[ j ] );
            field.get()->setPosition( make_pair( j, i ) );

            map[ i ].push_back( field );

            if( mapLine[ j ] == 'S' ){

                start.first = j;
                start.second = i;

            }
            if( mapLine [ j ] == 'F' ){

                finish.first = j;
                finish.second = i;

            }

        }
        i++;
    }
    mapFile.close();
    return true;

}

void Map::showMap ( WINDOW * window ) const {

    for( unsigned j = 0 ; j < map.size() ; j++ ) {

        for ( unsigned k = 0; k < map[ j ].size(); k++ ) {

            mvwprintw(  window, 3 + j , 6 + k, "%c", map[ j ][ k ].get()->getCharacter() );


        }

    }

}