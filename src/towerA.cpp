#include "towerA.hpp"

void TowerA::getHurt( int damage, Field & field ){

    field.getCharacter();
    life -= damage;

}

int TowerA::attack( shared_ptr< Field > & field, int & coins, int & killedAttackers ){

        if( field->getCharacter() == ATTACKER_ONE || field->getCharacter() == ATTACKER_TWO ) {

            field->getHurt( attackPower, *this );

            if( field->getLife() <= 0 ){

                killedAttackers += 1;
                coins += 15;

                shared_ptr< Field > newField = make_shared< Field >( Field() );
                newField.get()->setCharacter( ' ' );
                field = newField;
                return 1;

            }

        }

        return 0;

}