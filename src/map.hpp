#pragma once

#include <memory>
#include <vector>
#include <ncurses.h>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <utility>

#include "field.hpp"



using namespace std;

/**
 * Represent map for game.
 */

class Map{

public:

    /**
     * Default constructor.
     */
    Map() = default;
    /**
     * Default destructor.
     */
    ~Map() = default;

    /**
     * Load chosen map from file.
     *
     * @param chosenMap chosen map by player
     * @return return TRUE if loading map was successful, if not return FALSE
     */
    bool loadMap( int chosenMap );

    /**
     * Allows works with map
     *
     * @return reference to the map
     */
    vector< vector< shared_ptr< Field > > > & setMap( ){ return this->map; }
    /**
     * Getter of position of start.
     *
     * @return return position of start ( x,y )
     */
    pair< int ,int > getStart() const  { return start; }
    /**
     * Getter of position of finish.
     *
     * @return return position of finish ( x,y )
     */
    pair< int, int > getFinish() const { return finish; }

    /**
     * Show map into ncurses window.
     *
     * @param window window where map should be shown
     */
    void showMap ( WINDOW * window ) const ;

private:

    /** Position of start ( x, y )*/
    pair< int, int > start;
    /** Position of Finish ( x, y )*/
    pair< int, int > finish;
    /** Map saved in 2D array*/
    vector< vector< shared_ptr< Field > > > map;

};
