#pragma once

#include <list>
#include <utility>
#include <set>
#include <string>
#include <fstream>
#include <queue>
#include <map>
#include <algorithm>
#include "map.hpp"

using namespace std;

/**
 * Represent the path used by attacker to reach the finish through the map.
 */

class Path{

    public:

        /**
         * Default constructor.
         */
        Path() = default;

        /**
         * Default destructor.
         */
        ~Path() = default;

        /**
         * Find path through the map using algorithm, which is chosen by rand() function.
         *
         * @param start position of start
         * @param finish position of finish
         * @param map map of the game
         * @param chosenMap map chosen by player
         * @return return TRUE if path is found, if not FALSE
         */
        bool findPath( pair< int, int > start, pair< int, int > finish, Map & map , int chosenMap );
        /**
         * Getter that return found path.
         *
         * @return return path represented as list of ( x, y ) position
         */
        list< pair< int, int> > getPath () const { return path; }

         /**
          * Save path, position after position, into file for loading saved game.
          *
          * @return return TRUE if path is successfully saved, if not return FALSE
          */
        bool save();
        /**
         * Load path from file, position after position, using function predefinedPath();
         *
         * @return return TRUE if path is successfully load, if not return FALSE
         */
        bool load();

    private:

        /** Path represented by positions( x, y ).*/
        list< pair< int, int > > path;

        /**
         * Implemented BFS algorithm
         *
         * @param start position of start
         * @param finish position of finish
         * @param map chosen map
         * @return return TRUE if path is found, if not return FALSE
         */
        bool BFS ( pair< int, int > start, pair< int, int > finish, Map & map );
        /**
         * Find surrounding fields of one position on the map.
         *
         * @param lastVisitedField current position on the map
         * @param allChildren surrounding fields
         * @param allVisited all visited fields on the map
         * @param map map
         */
        void findChildren ( pair< int, int > lastVisitedField, set< pair< int, int > > & allChildren, set< pair< int, int > > & allVisited, Map & map );
        /**
         *  Read path from file, position after position.
         *
         * @param pathFile file in which is path saved
         * @return return TRUE if path is successfully load from file, if not return FALSE
         */
        bool predefinedPath ( ifstream & pathFile );
        /**
         * Implemented Greedy algorithm.
         *
         * @param start position of start
         * @param finish position of finish
         * @param map chosen map
         * @return return TRUE if path is found, if not return FALSE
         */
        bool heuristicAlgorithm( pair< int, int > start, pair< int, int > finish, Map & map );
        /**
         * Heuristic node used by greedy algorithm.
         */
        struct heuristicNode{

            pair< int, int > position;
            int heuristic;

        };
       /**
        * Sort heuristicNode by size of heuristic.
        */
        struct sorting {

            inline bool operator()( const heuristicNode A, const heuristicNode B ) {

                return A.heuristic < B.heuristic;

            }
        };
       /**
        * Calculate size of heuristic.
        *
        * @param start position of start
        * @param finish position of finish
        * @return return value of heuristic
        */
        int calculateHeuristic( pair< int, int > start, pair< int, int > finish );



};