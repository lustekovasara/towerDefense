#pragma once

#include "attacker.hpp"

using namespace std;

/**
 * Represent attacker one.
 */

class AttackerOne : public Attacker{

    public:

        /**
         * Default constructor.
         */
        AttackerOne () = default;
        /**
         * Constructor receiving life, character and attack power of attacker.
         *
         * @param life life of attacker
         * @param character character representing attacker
         * @param attackPower attack power of attacker
         */
        AttackerOne( int life, char character, int attackPower )
                : Attacker( life, character, attackPower ) {};

        /**
         * Default destructor.
         */
        ~AttackerOne() override = default;

        /**
         * Make copy of attacker.
         *
         * @return new copy of attacker
         */
        shared_ptr< Attacker > cloneAttacker () const override { return make_shared< AttackerOne > ( * this ); };

        /**
         * Take life of field.
         *
         * @param damage power of attack
         * @param field position of attacking field
         */
        void getHurt ( int damage, Field & field ) override;
        /**
         * Attack on the enemy of interacting object and addition/deduction of killed enemies.
         *
         * @param field field on which is attacking
         * @param coins coins of player
         * @param killedAttackers count of killed attackers
         * @return return 1 if object on field is killed, 0 if attack but not kill
         */
        int attack( shared_ptr< Field > & field , int & coins, int & killedAttackers ) override;

};
