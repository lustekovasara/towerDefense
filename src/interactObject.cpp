#include "interactObject.hpp"

int InteractObject::attack( shared_ptr<Field> & field, int & coins, int & killedAttackers ) {

    coins += 0;
    killedAttackers += 0;
    field->getHurt( attackPower, * this );
    return -1;

}

void InteractObject::save( ofstream &file ) {

    file << character<< "\n"
         << position.first << "\n"
         << position.second << "\n"
         << life << endl;

}