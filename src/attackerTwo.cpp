#include "attackerTwo.hpp"

void AttackerTwo::getHurt( int damage, Field & field ){


    if( field.getCharacter() == TOWER_B ){

        life -= ( damage / 5 );

    }
    else {

        life -= damage;

    }

}

int AttackerTwo::attack( shared_ptr< Field > & field , int & coins, int & killedAttackers ){

    int countOfShots = ( rand() % 2 ) + 2 ;

    field->getHurt( attackPower * countOfShots , * this );

    if( field->getLife() <= 0 ){

        killedAttackers -= 1;
        coins -= 1;

        shared_ptr< Field > newField = make_shared< Field >( Field() );
        newField.get()->setCharacter( ' ' );
        field = newField;
        return 1;

    }
    return 0;

}