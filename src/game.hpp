#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <ncurses.h>
#include <fstream>

#include "map.hpp"
#include "tower.hpp"
#include "towerA.hpp"
#include "towerB.hpp"
#include "towerC.hpp"
#include "attacker.hpp"
#include "attackerOne.hpp"
#include "attackerTwo.hpp"
#include "player.hpp"
#include "path.hpp"

using namespace std;

/**
 * Represent game and control every aspect of it ( control objects, menu, initialization, saving and loading game ).
 */

class Game{

public:

    /**
     * Default constructor.
     */
    Game() = default;
    /**
     * Destructor free memory of all towers and attackers.
     */
    ~Game(){ delete towerA; delete towerB; delete towerC; delete attackerOne; delete attackerTwo; }

    /**
     * Initializes all basics aspects of game.
     *
     * @return return TRUE if initialization was successful, if not return FALSE
     */
    bool gameInit();
    /**
     * Show menu on window ncurses.
     *
     * @param myChoice choice of player
     * @param stdscr window in which is menu shown
     */
    void showMenu( string & myChoice, WINDOW * stdscr );
    /**
     * Show options of map which can player chose, and also called functions which load map from file and find path.
     *
     * @param stdscr window in which is menu shown
     * @return return TRUE if map and path were loaded, if not return FALSE
     */
    bool selectMap( WINDOW * stdscr );

    /**
     * Control all aspects of game, move with attacker, start interaction between towers and attacker
     * and allows player to play game ( move, buy towers ).
     *
     * @param stdscr window in which is game shown
     */
    void runGame( WINDOW * stdscr );
    /**
     * Set game to the main settings. Clear all remains after last game.
     */
    void clearGame();

    /**
     * Saved all information about game.
     *
     * @return return TRUE if game is successfully saved, if not return FALSE
     */
    bool saveGame();
    /**
     * Clear all files in which are saved information about game.
     */
    void clearFiles();
    /**
     * Load saved game from files.
     *
     * @return return TRUE if game is successfully loade, if not return FALSE
     */
    bool loadGame();

    /**
     * Get information if game is running
     *
     * @return return TRUE if game is running, if not return FALSE
     */
    bool getIsRunning( ) const { return isRunning; }
    /**
     * Set if game is running or not.
     *
     * @param isRunning set state of game
     */
    void setIsRunning( bool isRunning ) { this->isRunning = isRunning; }
    /**
     * Get information if game is paused.
     *
     * @return return TRUE if game is paused, if not return FALSE
     */
    bool getIsPaused( ) const { return isPaused; }

private:

    /**Count of money, from which player can buy towers.*/
    int coins;
    /**Count of killed attackers.*/
    int killedAttackers = 0;
    /**Count of created attackers.*/
    int countOfAttacker = 0;
    /**Count of iterations of runGame().*/
    int count = 0;

    /**Model for making towers A, have all specifications.*/
    TowerA * towerA;
    /**Model for making towers B, have all specifications.*/
    TowerB * towerB;
    /**Model for making towers C, have all specifications.*/
    TowerC * towerC;
    /**Model for making attacker One, have all specifications.*/
    AttackerOne * attackerOne;
    /**Model for making attacker Two, have all specifications.*/
    AttackerTwo * attackerTwo;
    /**Array of placed towers.*/
    vector < shared_ptr< Tower > > towers;
    /**Array of attackers on map.*/
    vector < shared_ptr< Attacker > > attackers;
    /**Hold information if game is running.*/
    bool isRunning = true;
    /**Hold information if game is paused.*/
    bool isPaused = false;
    /**Hold information if player won.*/
    bool win = false;
    /**Hold information if player loss.*/
    bool loss = false;

    /**Chosen map by player.*/
    int chosenMap;
    /**Saved map.*/
    Map map;
    /**Player of the game.*/
    Player player;
    /**Path for chosen map.*/
    Path path;

    /**
     * Print logo in menu.
     *
     * @param WinPic window in which is logo printed
     */
    void showLogo( WINDOW * WinPic );
    /**
     * Print show on the screen using ncurses.
     *
     * @param window window in which is game printed
     */
    void showGame( WINDOW * window );

    /**
     * Interface for playing game ( moving, pause, placing towers ).
     *
     * @param key pressed key on the keyboard
     */
    void gameKeypad( int key );
    /**
     * Start all interactions between attackers and towers.
     */
    void startInteraction();
    /**
     * Show results of game.
     *
     * @param winMap window in which is result printed.
     */
    void showResult( WINDOW * winMap );

    /**
     * Takes care of initialization of basics of the game.
     *
     * @return return TRUE if basics were initialized successfully, if not return FALSE
     */
    bool basicsInit();
    /**
     * Takes care of initialization of towers.
     *
     * @return return TRUE if towers were initialized successfully, if not return FALSE
     */
    bool towersInit();
    /**
     * Read information about towers from file.
     *
     * @param initFile file, from which are towers loaded
     * @param lifeInit life of tower
     * @param attackPowerInit attack power of tower
     * @param priceInit price of tower
     * @param characterInit character representing tower
     * @return return TRUE if reading was successful, if not return FALSE
     */
    bool readFromTowersInitFile ( ifstream & initFile, int & lifeInit, int & attackPowerInit, int & priceInit, char & characterInit );
    /**
     * Takes care of initialization of attackers.
     *
     * @return return TRUE if attackers were initialized successfully, if not return FALSE
     */
    bool attackersInit();
    /**
     * Read information about attackers from file.
     *
     * @param initFile file, from which are attackers loaded
     * @param lifeInit life of attacker
     * @param attackPowerInit attack power of attacker
     * @param characterInit character representing attacker
     * @return return TRUE if reading was successful, if not return FALSE
     */
    bool readFromAttackersInitFile( ifstream & initFile, int & lifeInit, int & attackPowerInit, char & characterInit );

    /**
     * Order all towers to attack.
     */
    void towerAttack();
    /**
     * Order all attacker to move or attack.
     */
    void attackerInteract();
    /**
     * Generate new attackers.
     */
    void generateAttacker();

    /**
     * Save basic information about game into file.
     *
     * @return return TRUE if saving was successful, if not return FALSE
     */
    bool saveBasics();
    /**
     * Save information about towers into file.
     *
     * @return return TRUE if saving was successful, if not return FALSE
     */
    bool saveTowers();
    /**
     * Save information about attackers into file.
     *
     * @return return TRUE if saving was successful, if not return FALSE
     */
    bool saveAttackers();

    /**
     * Load basic information about game from file.
     *
     * @return return TRUE if loading was successful, if not return FALSE
     */
    bool loadBasics();
    /**
      * Load information about towers from file.
      *
      * @return return TRUE if loading was successful, if not return FALSE
      */
    bool loadTowers();
    /**
      * Load information about attacker from file.
      *
      * @return return TRUE if loading was successful, if not return FALSE
      */
    bool loadAttackers();

};