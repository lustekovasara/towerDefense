#pragma once

#include <memory>
#include "map.hpp"

#define ATTACKER_ONE '@'
#define ATTACKER_TWO '$'
#define TOWER_A 'A'
#define TOWER_B 'B'
#define TOWER_C 'C'

using namespace std;

/**
 * Represent interacting objects.
 */

class InteractObject : public Field {

    public:

        /**
         * Default constructor.
         */
        InteractObject() = default;
        /**
         * Constructor receiving life and character of field and attack power of interacting object.
         *
         * @param life life of field
         * @param character character representing field
         * @param attackPower attack power of interacting object
         */
        InteractObject( int life, char character, int attackPower )
                        : Field( life, character ), attackPower ( attackPower ) {};


        /**
         * Attack on the enemy of interacting object and addition/deduction of killed enemies.
         *
         * @param field field on which is attacking
         * @param coins coins of player
         * @param killedAttackers count of killed attackers
         * @return return 1 if object on field is killed, 0 if attack but not kill
         */
        virtual int attack ( shared_ptr< Field > & field , int & coins, int & killedAttackers );

        /**
         * Save information in file, about interacting object.
         *
         * @param file file in which information will be saved
         */
        void save( ofstream & file );

    protected:

        /** Attack power of interacting object.*/
        int attackPower = 0;


};