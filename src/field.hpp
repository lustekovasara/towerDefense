#pragma once

#include <utility>


using namespace std;

/**
 * Represent field of map saved in 2D array.
 */

class Field{

    public:

        /**
         * Defaul constructor
         */
        Field() = default;
        /**
         * Constructor receiving character representing field and position of field.
         *
         * @param character character representing field
         * @param position position of field ( x, y )
         */
        Field( char character, pair< int, int > position );
        /**
         * Constructor receiving life and character representing field.
         *
         * @param life life of field
         * @param character character representing field
         */
        Field( int life, char character )
                : life ( life ), character ( character ) {};

        /**
         * Default destructor
         */
        virtual ~Field() = default;

        /**
         * Getter of character.
         *
         * @return return character representing field
         */
        char getCharacter() const { return character; }
        /**
         * Set character of field.
         *
         * @param character character representing field
         */
        void setCharacter ( char character ) { this ->character = character; }
        /**
         * Getter of life.
         *
         * @return return life of field
         */
        int getLife () const { return life; }
        /**
         * Set life of field.
         *
         * @param life new life
         */
        void setLife ( int life ) { this->life = life; }
        /**
         * Getter of position of the field.
         *
         * @return position of the field ( x, y )
         */
        pair< int, int > getPosition () const { return position; }
        /**
         * Set position of the field.
         *
         * @param position position of field ( x, y )
         */
        void setPosition ( pair< int, int > position ) { this->position = position; }

        /**
         * Take life of field.
         *
         * @param damage power of attack
         * @param field position of attacking field
         */
        virtual void getHurt ( int damage, Field & field );

    protected:

        /** Position of field ( x, y ).*/
        pair< int, int > position;
        /** Life of field.*/
        int life = 0;
        /** Character representing the field.*/
        char character;


};
