#include "path.hpp"


bool Path::findPath( pair< int, int > start, pair< int, int > finish, Map & map, int chosenMap ) {

    bool returnValue = false;
    int option = rand() % 3;

     path.clear();

     if( option == 0 ){

         returnValue = BFS( start, finish , map );
         path.pop_front();


     }
     else if( option == 1 ){

         returnValue = heuristicAlgorithm( start, finish, map );
         path.pop_front();

     }
     else if( option == 2 ){

         ifstream pathFile;
         pathFile.open( "src/files/mapPath" + to_string( chosenMap ) + ".txt"); // budu nacitat bud odsud a nebo v pripade load

         if( !pathFile. is_open() ){

             return false;

         };
         if( !pathFile.good() ){

             pathFile.close();
             return false;

         }

         returnValue = predefinedPath( pathFile );
         pathFile.close();
     }

    return returnValue;

}

bool Path::BFS ( pair< int, int > start, pair< int, int > finish, Map & map ) {

    queue< pair< int, int > > queueOfFields;

    set< pair< int, int > > allVisited;
    list< pair< int, int > > visited;

    multimap< pair< int, int >, list< pair< int, int > > > fieldWithRoute;
    list< pair< int, int > > emptyList;

    pair< int, int > lastVisitedField;
    list< pair< int, int > > lastVisitedFieldList;

    if( start == finish ){

        visited.push_back( start );
        path = visited;
        return true;

    }

    queueOfFields.push( start );
    fieldWithRoute.insert(  pair< pair< int, int >, list< pair< int, int > > > ( start, emptyList ) );

    while( !queueOfFields.empty() ){

        lastVisitedField = queueOfFields.front();
        lastVisitedFieldList = fieldWithRoute.find( lastVisitedField )->second;

        queueOfFields.pop();

        if( allVisited.find(lastVisitedField ) != allVisited.end() ) {

            continue;

        }

        lastVisitedFieldList.push_back( lastVisitedField );

        if( lastVisitedField == finish ){

            path = lastVisitedFieldList;
            return true;

        }

        set< pair< int, int > > allChildren;

        findChildren( lastVisitedField, allChildren, allVisited, map );
        for ( auto iter = allChildren.begin() ; iter != allChildren.end() ; ++iter ){

            if ( allVisited.find( * iter ) != allVisited.end() ){

                continue;

            }

            fieldWithRoute.insert( pair< pair< int, int >, list< pair< int, int > > > ( *iter, lastVisitedFieldList ) );
            queueOfFields.push( * iter );


        }
        allVisited.insert( lastVisitedField );

    }
    return false;
}

void Path::findChildren ( pair< int, int > lastVisitedField, set< pair< int, int > > & allChildren, set< pair< int, int > > & allVisited, Map & map ){

    int x = lastVisitedField.first;
    int y = lastVisitedField.second;

    if ( map.setMap()[ y - 1 ][ x ].get()->getCharacter() == '#' ){

        allVisited.insert( make_pair( x, y - 1 ));

    }
    allChildren.insert( make_pair( x, y - 1 ) );

    if ( map.setMap()[ y + 1 ][ x ].get()->getCharacter() == '#' ){

        allVisited.insert( make_pair( x, y + 1 ));

    }
    allChildren.insert( make_pair( x, y + 1 ) );

    if ( map.setMap()[ y ][ x - 1 ].get()->getCharacter() == '#' ){

        allVisited.insert( make_pair( x - 1 , y ) );

    }
    allChildren.insert( make_pair( x - 1 , y ) );

    if ( map.setMap()[ y ][ x + 1 ].get()->getCharacter() == '#' ){

        allVisited.insert( make_pair( x + 1 , y  ) );

    }
    allChildren.insert( make_pair( x + 1 , y ) );

}

bool Path::predefinedPath ( ifstream & pathFile ) {

    string line;
    int x, y;

    while( true ){

        if ( !getline( pathFile, line ) ){

            break;

        }
        x = stoi( line );

        if ( !getline( pathFile, line ) ){

            return false;

        }
        y = stoi( line );

        path.push_back( make_pair( x, y ) );

    }

    return true;

}

int Path::calculateHeuristic( pair< int, int > start, pair< int, int > finish ){

    return abs( start.first - finish.first ) + abs( start.second - finish.second );

}


bool Path::heuristicAlgorithm( pair< int, int > start, pair< int, int > finish, Map & map ){

   vector< heuristicNode > vectorOfFields;

    set< pair< int, int > > allVisited;
    list< pair< int, int > > visited;

    multimap< pair< int, int >, list< pair< int, int > > > fieldWithRoute;
    list< pair< int, int > > emptyList;

    pair< int, int > lastVisitedField;
    list< pair< int, int > > lastVisitedFieldList;

    if( start == finish ){

        visited.push_back( start );
        path = visited;
        return true;

    }

    heuristicNode tmp;
    tmp.position = start;
    tmp.heuristic = calculateHeuristic( start, finish );

    vectorOfFields.push_back( tmp );
    fieldWithRoute.insert(  pair< pair< int, int >, list< pair< int, int > > > ( start, emptyList ) );

    while( !vectorOfFields.empty() ){

        sort( vectorOfFields.begin(), vectorOfFields.end(), sorting() );
        lastVisitedField = vectorOfFields[ 0 ].position;
        lastVisitedFieldList = fieldWithRoute.find( lastVisitedField )->second;

        vectorOfFields.erase( vectorOfFields.begin() );

        if( allVisited.find(lastVisitedField ) != allVisited.end() ) {

            continue;

        }

        lastVisitedFieldList.push_back( lastVisitedField );

        if( lastVisitedField == finish ){

            path = lastVisitedFieldList;
            return true;

        }

        set< pair< int, int > > allChildren;

        findChildren( lastVisitedField, allChildren, allVisited, map );

        for ( auto iter = allChildren.begin() ; iter != allChildren.end() ; ++iter ){

            if ( allVisited.find( * iter ) != allVisited.end() ){

                continue;

            }

            fieldWithRoute.insert( pair< pair< int, int >, list< pair< int, int > > > ( *iter, lastVisitedFieldList ) );
            heuristicNode tmp2;
            tmp2.position = *iter;
            tmp2.heuristic = calculateHeuristic( *iter, finish );
            vectorOfFields.push_back( tmp2 );


        }
        allVisited.insert( lastVisitedField );

    }
    return false;


}

bool Path::save() {

    ofstream pathFile;
    pathFile.open( "src/files/savePath.txt", ofstream::trunc );
    if( !pathFile. is_open() ){

        return false;

    };
    if( !pathFile.good() ) {

        pathFile.close();
        return false;

    }

    list< pair< int, int > >::iterator iter;
    for ( iter = path.begin() ; iter != path.end(); iter++ ){

        pathFile << iter->first << "\n"
                 << iter->second << endl;

    }

    pathFile.close();
    return true;

}

bool Path::load() {

    path.clear();

    ifstream pathFile;
    pathFile.open( "src/files/savePath.txt" );
    if( !pathFile. is_open() ){

        return false;

    };
    if( !pathFile.good() ) {

        pathFile.close();
        return false;

    }

    if( !predefinedPath( pathFile ) ){

        pathFile.close();
        return false;

    }

    pathFile.close();
    return true;

}
