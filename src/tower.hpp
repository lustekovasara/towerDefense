#pragma once

#include <vector>
#include <memory>
#include <ncurses.h>

#include "interactObject.hpp"

using namespace std;

/**
 * Represent the tower and its interaction.
 */

class Tower: public InteractObject {

    public:

        /**
         * Default constructor.
         */
        Tower() = default;
        /**
         * Constructor receiving life, character, attack power and price of tower.
         *
         * @param life life of tower
         * @param character character representing tower
         * @param attackPower attack power of tower
         * @param price price of tower
         */
        Tower( int life, char character, int attackPower, int price )
                : InteractObject( life, character, attackPower ), price ( price ) {};

        /**
         * Default destructor.
         */
        virtual ~Tower() = default;

        /**
         * Make copy of tower.
         *
         * @return new copy of tower
         */
        virtual shared_ptr< Tower > cloneTower () const { return make_shared< Tower >( * this ); }
        /**
         * Get price of tower.
         *
         * @return price of tower
         */
        int getPrice() { return price; }
        /**
         * Set the attacking area depending on position of tower.
         *
         * @param x position of the tower on the x axis
         * @param y position of the tower on the y axis
         */
        void setAttackingArea( int x, int y );
        /**
         * Get attacking arrea of tower.
         *
         * @return fields ( x, y ) of attacking area
         */
        vector< pair< int, int > > getAttackingArea() { return attackingArea; }
        /**
         * Print info about tower into window ncurses.
         *
         * @param window window in which are information printed
         */
        void showInfo ( WINDOW * window ) const;

    protected:

        /**Position ( x,y ) of field in attacking area.*/
        vector< pair< int, int > > attackingArea;
        /**Price of tower.*/
        int price;



};