#include <iostream>
#include <ncurses.h>
#include <string>

#include "game.hpp"

using namespace std;

/**
 * Creates new Game and allows its control thru menu.
 */

int main( void ) {

    Game * game = new Game( ) ;

    initscr();
    noecho();
    curs_set(0);
    string choice;
    bool selectedMap = false;

    while ( true ) {

        game->showMenu( choice, stdscr );

        if ( choice == "Select map" ) {

            game->clearGame();
            if( !game->gameInit() ) {

                cout << "Game initialization failed." << endl;
                delete game;
                return 0;

            }
            if ( !game->selectMap( stdscr ) ) {

                cout << "Loading map failed" << endl;
                delete game;
                return 0;

            }
            selectedMap = true;

        }
        else if ( choice == "Start" ) {


            if ( selectedMap == false ) {

                cout << "Map is not selected" << endl;

            }
            else {

                game->setIsRunning( true );

                while ( game->getIsRunning() ) {

                    game->runGame( stdscr );

                }

            }

        }
        else if( choice == "Save game" ){

            if ( !selectedMap || !game->getIsPaused() ) {

                cout << "There is no game to save" << endl;

            }
            else{

                if( !game->saveGame() ){

                    game->clearFiles();

                    cout << "Saving game failed." << endl;
                    delete game;
                    return 0;

                }

            }

        }
        else if( choice == "Load game" ){

            game->clearGame();
            if( !game->gameInit() ) {

                cout << "Game initialization failed." << endl;
                delete game;
                return 0;

            }

            if( !game->loadGame() ) {


                cout << "Game loading failed." << endl;
                delete game;
                return 0;

            }
            selectedMap = true;

        }
        else if ( choice == "Exit" ) {

            break;

        }

    }

    endwin();
    delete game;
    return 0;
}