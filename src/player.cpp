#include "player.hpp"

Player::Player( int x, int y, char character ) {

    position = make_pair( x, y );
    this->character = character;

}

void Player::move( int x, int y ) {

    position.first += x;
    position.second += y;

}

void Player::save( ofstream & file ) {

    file << position.first << "\n"
         << position.second << "\n"
         << character << endl;

}

bool Player::load( ifstream & file ) {

    string readString;

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    position.first = stoi( readString );

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    position.second = stoi( readString );

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    character = readString[ 0 ];

    return true;

}
