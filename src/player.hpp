#pragma once

#include <utility>
#include <fstream>

using namespace std;

/**
 * Represents player of the game.
 */
class Player {

    public:

        /**
         * Default constructor.
         */
        Player() = default;

        /**
         * Constructor receiving position and character which represent player on the map.
         *
         * @param x position on the map by x axis
         * @param y position on the map by y axis
         * @param character character which represent player
         */
        Player ( int x, int y, char character );

        /**
         * Default destructor.
         */
        ~Player() = default;

        /**
         * Moves the player position on the map.
         *
         * @param x value of steps on the map by x axis
         * @param y value of steps on the map by y axis
         */
        void move ( int x, int y );

        /**
         * Getter which returns character representing the player.
         *
         * @return character which represent the player
         */
        char getCharacter() const { return character; }
        /**
         * Getter which return position on player.
         *
         * @return position of player ( pair x, y )
         */
        pair< int, int > getPosition () const { return position; }

        /**
         * Save all information about player into file for loading saved game.
         *
         * @param file the file to which the information about player is stored
         */
        void save( ofstream & file );
        /**
         * Load all information about player from file in which are saved information about saved game.
         *
         * @param file the file from which the information about player is loading
         * @return TRUE if loading from files was successful, when not return FALSE
         */
        bool load( ifstream & file );

    private:

        /** Position on player on the map ( x, y ).*/
        pair< int, int > position;
        /**  Character representing the player.*/
        char character;

};