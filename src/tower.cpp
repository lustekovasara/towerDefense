#include "tower.hpp"

void Tower::showInfo( WINDOW * window ) const {

    wattron( window, A_UNDERLINE );
    mvwprintw( window, 1, 1, "Tower: %c", character );
    wattroff( window, A_UNDERLINE );
    mvwprintw( window, 3, 1, "Price: %d", price );
    mvwprintw( window, 4, 1, "Life: %d", life );
    mvwprintw( window, 5, 1, "Attack power: %d ", attackPower );


}

void Tower::setAttackingArea( int x, int y ) {

    position.first = x;
    position.second = y;

    attackingArea.push_back( make_pair< int, int >( x - 1, y - 1 ) );
    attackingArea.push_back( make_pair< int, int >( x + 0, y - 1 ) );
    attackingArea.push_back( make_pair< int, int >( x + 1, y - 1 ) );
    attackingArea.push_back( make_pair< int, int >( x - 1, y + 0 ) );
    attackingArea.push_back( make_pair< int, int >( x + 1, y + 0 ) );
    attackingArea.push_back( make_pair< int, int >( x - 1, y + 1 ) );
    attackingArea.push_back( make_pair< int, int >( x + 0, y + 1 ) );
    attackingArea.push_back( make_pair< int, int >( x + 1, y + 1 ) );

}