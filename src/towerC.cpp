#include "towerC.hpp"

void TowerC::getHurt( int damage, Field & field ) {

    if( field.getCharacter() == ATTACKER_ONE ){

        life -= ( damage / 2 );

    }
    if( field.getCharacter() == ATTACKER_TWO ){

        life -= ( damage / 3 );

    }
    else {

        life -= damage;

    }

}

int TowerC::attack( shared_ptr< Field > & field, int & coins, int & killedAttackers ) {

    int random = rand() % 2;

    if( random == 1 ){

        if( field->getCharacter() == ATTACKER_ONE || field->getCharacter() == ATTACKER_TWO ) {

            field->getHurt( attackPower, * this );

            if( field->getLife() <= 0 ){

                killedAttackers += 1;
                coins += 25;

                shared_ptr< Field > newField = make_shared< Field >( Field() );
                newField.get()->setCharacter( ' ' );
                field = newField;

            }

        }

    }
    return 0;

}