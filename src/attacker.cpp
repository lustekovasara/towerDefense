#include "attacker.hpp"


int Attacker::move( Map & map ) {

    int x = path.front().first;
    int y = path.front().second;

    if( map.setMap()[ y ][ x ].get()->getCharacter() == ' ' ){

        int xOld = position.first;
        int yOld = position.second;

        position.first = x;
        position.second = y;

        map.setMap()[ y ][ x ] = this->cloneAttacker();

        if( map.setMap()[ yOld ][ xOld ].get()->getCharacter() != 'S' ) {

            shared_ptr< Field > field = make_shared< Field >( Field() );
            field.get()->setCharacter( ' ' );
            map.setMap()[ yOld ][ xOld ] = field;

        }

        path.pop_front();

        return 0;

    }
    if( map.setMap()[ y ][ x ].get()->getCharacter() == TOWER_A ||
        map.setMap()[ y ][ x ].get()->getCharacter() == TOWER_B ||
        map.setMap()[ y ][ x ].get()->getCharacter() == TOWER_C ){

        return 1;

    }
    if( map.getFinish().first == x && map.getFinish().second == y ){

        return 2;

    }

    return -1;

}

void Attacker::loadPositionInPath( int x, int y ){

    int i = 0;
    while( !path.empty() ){

        if( path.begin()->first == x && path.begin()->second == y ){

            path.pop_front();
            break;

        }
        path.pop_front();
        i++;

    }

}