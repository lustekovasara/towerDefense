#include "towerB.hpp"

void TowerB::getHurt( int damage, Field & field  ) {

    field.getPosition();

    damage = ( damage / 4 );
    life -= damage;

}

int TowerB::attack( shared_ptr< Field > & field, int & coins, int & killedAttackers ) {

    int countOfShots = ( rand() % 2 ) + 1 ;

    if( field->getCharacter() == ATTACKER_ONE || field->getCharacter() == ATTACKER_TWO ) {

        for ( int i = 0; i < countOfShots; i++ ) {

            field->getHurt( attackPower, *this );

            if ( field->getLife() <= 0 ) {

                killedAttackers += 1;
                coins += 20;

                shared_ptr< Field > newField = make_shared< Field >( Field() );
                newField.get()->setCharacter( ' ' );
                field = newField;
                return 1;

            }

        }

    }
    return 0;

}