#include "field.hpp"

Field::Field( char character, pair<int, int> position ) {

    this->character = character;
    this->position = position;

}

void Field::getHurt( int damage, Field & field ) {

    if( field.getCharacter() ){

        damage = 0;
        life -= damage;

    }

}