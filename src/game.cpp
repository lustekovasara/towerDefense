#include "game.hpp"
#define MAX_COUNT_OF_ATTACKERS 20
#define KILLED_ATTACKERS_FOR_WIN 10

using namespace std;

bool Game::gameInit() {


    if( !towersInit() || !attackersInit() || !basicsInit() ){

        return false;

    }

    return true;

}

bool Game::basicsInit(){

    ifstream initFile;
    string initString;
    int x, y;
    char character;

    initFile.open( "src/files/init.txt" );
    if( !initFile. is_open() ){

        return false;

    };
    if( !initFile.good() ){

        initFile.close();
        return false;

    }

    if ( !getline( initFile, initString ) ){

        initFile.close();
        return false;

    }
    coins = stoi( initString );

    if ( !getline( initFile, initString ) ){

        initFile.close();
        return false;

    }
    x = stoi( initString );

    if ( !getline( initFile, initString ) ){

        initFile.close();
        return false;

    }
    y = stoi( initString );

    if ( !getline( initFile, initString ) ){

        initFile.close();
        return false;

    }
    character = initString[ 0 ];

    player = Player( x, y, character );

    initFile.close();
    return true;

}

bool Game::towersInit(){

    int lifeInit;
    int attackPowerInit;
    int priceInit;
    char characterInit;

    ifstream initFile;

    initFile.open( "src/files/towersInit.txt" );
    if( !initFile. is_open() ){

        return false;

    };
    if( !initFile.good() ){

        initFile.close();
        return false;

    };

    if ( !readFromTowersInitFile( initFile, lifeInit, attackPowerInit, priceInit, characterInit ) ){

        initFile.close();
        return false;

    }
    towerA = new TowerA( lifeInit, characterInit, attackPowerInit, priceInit ); //nezapomenout v destructoru uvolnit

    if ( !readFromTowersInitFile( initFile, lifeInit, attackPowerInit, priceInit, characterInit ) ){

        initFile.close();
        return false;

    }
    towerB = new TowerB( lifeInit, characterInit, attackPowerInit, priceInit  );

    if ( !readFromTowersInitFile( initFile, lifeInit, attackPowerInit, priceInit, characterInit ) ){

        initFile.close();
        return false;

    }
    towerC = new TowerC( lifeInit, characterInit, attackPowerInit, priceInit );

    initFile.close();
    return true;

}

bool Game::readFromTowersInitFile ( ifstream & initFile , int & lifeInit, int & attackPowerInit, int & priceInit, char & characterInit ){

    string initString;

    if( !getline( initFile, initString ) ){

        return false;

    }
    lifeInit = stoi( initString );

    if( !getline( initFile, initString ) ){

        return false;

    }
    attackPowerInit = stoi( initString );

    if( !getline( initFile, initString ) ){

        return false;

    }
    priceInit = stoi( initString );

    if( !getline( initFile, initString ) ){

        return false;

    }
    characterInit = initString[ 0 ];

    return true;

}

bool Game::attackersInit(){

    int lifeInit;
    int attackPowerInit;
    char characterInit;

    ifstream initFile;

    initFile.open( "src/files/attackersInit.txt" );
    if( !initFile. is_open() ){

        return false;

    };
    if( !initFile.good() ){

        initFile.close();
        return false;

    };

    if ( !readFromAttackersInitFile( initFile, lifeInit, attackPowerInit, characterInit ) ){

        initFile.close();
        return false;

    }
    attackerOne = new AttackerOne( lifeInit, characterInit, attackPowerInit );

    if ( !readFromAttackersInitFile( initFile, lifeInit, attackPowerInit, characterInit ) ){

        initFile.close();
        return false;

    }
    attackerTwo = new AttackerTwo( lifeInit, characterInit, attackPowerInit );

    initFile.close();
    return true;
}

bool Game::readFromAttackersInitFile( ifstream &initFile, int &lifeInit, int &attackPowerInit, char &characterInit ) {

    string initString;

    if( !getline( initFile, initString ) ){

        return false;

    }
    lifeInit = stoi( initString );

    if( !getline( initFile, initString ) ){

        return false;

    }
    attackPowerInit = stoi( initString );

    if( !getline( initFile, initString ) ){

        return false;

    }
    characterInit = initString[ 0 ];

    return true;

}

void Game::showMenu( string & myChoice, WINDOW * stdscr ) {

    curs_set(0);
    raw();

    int yMax, xMax, i;
    getmaxyx( stdscr, yMax, xMax );

    WINDOW * winMenu = newwin( 9, xMax - 30, ( yMax / 2 ) , 15 );
    WINDOW * winPic = newwin( 16, xMax - 30, 5 , 15 );

    Game::showLogo( winPic );
    keypad( winMenu, true );

    string options[5] = { "Select map", "Start", "Save game", "Load game", "Exit" };
    int choice, current = 0;

    while( true ){

        for( i = 0 ; i < 5 ; i++ ){

            if( i == current ){

                wattron( winMenu, A_REVERSE );

            }
            getmaxyx( winMenu, yMax, xMax );
            mvwprintw( winMenu, 2 + i, ( xMax / 2 ) - 5 , options[ i ].c_str() );
            wattroff( winMenu, A_REVERSE );
            mvwprintw( winMenu, yMax - 1 , xMax - 1, " ");

        }
        choice = wgetch( winMenu );

        switch ( choice ){

            case KEY_UP:
                current--;
                if( current == -1 ){ current = 4; }
                break;
            case KEY_DOWN:
                current++;
                if( current == 5 ){ current = 0; }
                break;
            default:
                break;
        }
        if( choice == 10 ) {//enter

            myChoice = options[current];
            break;

        }

    }
    wclear( winMenu );
    wclear( winPic );
    wrefresh( winMenu );
    wrefresh( winPic );
    delwin( winMenu );
    delwin( winPic);

}

void Game::showLogo( WINDOW * winPic ) {

    std::string logo [12] = {"        <|",
                             "     _  _|_  _",
                             "    | |_| |_| |",
                             "    \\\\        /",
                             "     \\\\      /",
                             "      ||    |       _       __",
                             "      ||    |      | |     / _|",
                             "      ||    |    __| | ___| |_ ___ _ __  ___  ___",
                             "      ||    |   / _` |/ _ \\  _/ _ \\ '_ \\/ __|/ _ \\",
                             "      ||    |  | (_| |  __/ ||  __/ | | \\__ \\  __/",
                             "      ||    |   \\__,_|\\___|_| \\___|_| |_|___/\\___|",
                             "__________________________________________________"};

    int xMax = getmaxx( winPic );
    for( int i = 0 ; i < 12 ; i++ ){

        mvwprintw( winPic, 1 + i, ( xMax / 2 ) - 25, logo[ i ].c_str() );

    }
    wrefresh( winPic );

}

bool Game::selectMap( WINDOW * stdscr ){

    curs_set(0);
    noecho( );
    raw();

    int yMax, xMax, i;
    getmaxyx( stdscr, yMax, xMax );

    WINDOW * winMap = newwin( yMax - 20 , xMax - 30, 10 , 15 );
    wrefresh( winMap );

    string options[ 3 ] = { "Map 1", "Map 2", "Map 3" };
    int choice, current = 0;

    keypad( winMap, true );
    while( true ){

        for( i = 0 ; i < 3 ; i++ ){

            if( i == current ){

                wattron( winMap, A_REVERSE );

            }
            getmaxyx( winMap, yMax, xMax );
            mvwprintw( winMap, ( yMax / 2 ) - 2 + i, ( xMax / 2 ) - 3 , options[ i ].c_str() );
            wattroff( winMap, A_REVERSE );

        }
        choice = wgetch( winMap );

        switch ( choice ){

            case KEY_UP:
                current--;
                if( current == -1 ){ current = 2; }
                break;
            case KEY_DOWN:
                current++;
                if( current == 3 ){ current = 0; }
                break;
            default:
                break;

        }
        if( choice == 10 ){ break; } // enter

    }
    //---------------- tohle dat i do nacitani hry
    if( !map.loadMap( current + 1 ) || !path.findPath( map.getStart(), map.getFinish(), map ,current + 1 ) ){

        curs_set(1);
        wclear( winMap );
        wrefresh( winMap );
        endwin();
        return false;

    }
    attackerOne->setPath( path.getPath() );
    attackerTwo->setPath( path.getPath() );

    chosenMap = current + 1;

    curs_set(1);
    wclear( winMap );
    wrefresh( winMap );
    delwin( winMap );
    return true;

}

void Game::runGame( WINDOW * stdscr ) {

    curs_set( 0 );
    noraw();
    halfdelay( 2 );
    keypad( stdscr, true );
    isPaused = false;

    WINDOW * winMap = newwin( 28, 80, 3, 10 );
    WINDOW * winGameResult = newwin( 7, 20, 3, 90 );
    WINDOW * winTower1 = newwin( 7, 20, 10, 90 );
    WINDOW * winTower2 = newwin( 7, 20, 17, 90 );
    WINDOW * winTower3 = newwin( 7, 20, 24, 90 );

    box ( winTower1, 0, 0 );
    box ( winTower2, 0, 0 );
    box ( winTower3, 0, 0 );
    box ( winMap, 0, 0 );
    box ( winGameResult, 0 , 0 );

    int key;
    if( ( key = getch( ) ) != ERR ){

        gameKeypad( key );

    }

    if( count == 0 && !isPaused ){

        startInteraction();

    }

    count = ( ( count + 1 ) % 5 );

    towerA->showInfo( winTower1 );
    towerB->showInfo( winTower2 );
    towerC->showInfo( winTower3 );
    showGame( winGameResult );
    map.showMap( winMap );
    mvwprintw( winMap, 3 + player.getPosition().second, 6 + player.getPosition().first, "%c", player.getCharacter() );

    wrefresh( winMap );
    wrefresh( winGameResult );
    wrefresh( winTower1 );
    wrefresh( winTower2 );
    wrefresh( winTower3 );

   if( getIsPaused() || win || loss ) {

       showResult( winMap );

       isRunning = false;

       wclear( winMap );
       wclear( winGameResult );
       wclear( winTower1 );
       wclear( winTower2 );
       wclear( winTower3 );
       clear();
       wrefresh( winMap );
       wrefresh( winGameResult );
       wrefresh( winTower1 );
       wrefresh( winTower2 );
       wrefresh( winTower3 );
       refresh();
       delwin( winMap );
       delwin( winGameResult );
       delwin( winTower1 );
       delwin( winTower2 );
       delwin( winTower3 );

   }
   else {

       delwin(winMap);
       delwin(winGameResult);
       delwin(winTower1);
       delwin(winTower2);
       delwin(winTower3);

   }


}

void Game::gameKeypad( int key ) {

    int x = player.getPosition().first;
    int y = player.getPosition().second;

    switch ( key ){

        case KEY_UP:
            if( map.setMap()[ y - 1 ][ x ].get()->getCharacter() != '#' ) {

                player.move( 0, -1 );

            }
            break;
        case KEY_RIGHT:
            if( map.setMap()[ y ][ x + 1 ].get()->getCharacter() != '#' ) {

                player.move( 1, 0 );

            }
            break;
        case KEY_DOWN:
            if( map.setMap()[ y + 1 ][ x ].get()->getCharacter() != '#' ) {

                player.move( 0, 1 );

            }
            break;
        case KEY_LEFT:
            if( map.setMap()[ y ][ x - 1 ].get()->getCharacter() != '#' ) {

                player.move( -1, 0 );

            }
            break;
        case 'a':
            if( map.setMap()[ y ][ x ].get()->getCharacter() == ' ' ) {

                if( ( coins - towerA->getPrice() ) >= 0 ) {

                    shared_ptr <Tower> placeTower = towerA->cloneTower();
                    placeTower.get()->setAttackingArea( x, y );
                    map.setMap()[ y ][ x ] = placeTower;
                    towers.push_back( placeTower );
                    coins -= towerA->getPrice();

                }

            }
            break;
        case 'b':
            if( map.setMap()[ y ][ x ].get()->getCharacter() == ' ' ) {

                if( ( coins - towerB->getPrice() ) >= 0 ) {

                    shared_ptr <Tower> placeTower = towerB->cloneTower();
                    placeTower.get()->setAttackingArea( x, y );
                    map.setMap()[ y ][ x ] = placeTower;
                    towers.push_back( placeTower );
                    coins -= towerB->getPrice();

                }

            }
            break;
        case 'c':
            if( map.setMap()[ y ][ x ].get()->getCharacter() == ' ' ) {

                if( ( coins - towerC->getPrice() ) >= 0 ) {

                    shared_ptr <Tower> placeTower = towerC->cloneTower();
                    placeTower.get()->setAttackingArea( x, y );
                    map.setMap()[ y ][ x ] = placeTower;
                    towers.push_back( placeTower );
                    coins -= towerC->getPrice();

                }

            }
            break;
        case 'p':
            isPaused = true;
            break;
        default:
            break;

    }

}

void Game::startInteraction() {

    towerAttack();
    attackerInteract();

    if( countOfAttacker < MAX_COUNT_OF_ATTACKERS ){

        generateAttacker();

    }
    if( ( killedAttackers >= KILLED_ATTACKERS_FOR_WIN || ( countOfAttacker == MAX_COUNT_OF_ATTACKERS && attackers.empty() ) ) && !loss ){

        win = true;

    }
    if( towers.empty() && coins == 0 && !win ){

        loss = true;

    }

}

void Game::showResult( WINDOW *winMap ) {

    halfdelay( 20 );
    wclear( winMap );

    box( winMap, 0, 0 );
    int xMax, yMax;
    getmaxyx( winMap, yMax, xMax );

    wattron( winMap, A_REVERSE );
    if( loss ){

        mvwprintw( winMap, ( yMax / 2 ) - 2, ( xMax / 2 ) - 5, "Game Over" );

    }
    else if( win ){

        mvwprintw( winMap, ( yMax / 2 ) - 2 , ( xMax / 2 ) - 8, "Congratulations!" );

    }
    else if( isPaused ){

        mvwprintw( winMap, ( yMax / 2 ) - 2 , ( xMax / 2 ) - 3, "Pause" );

    }
    wattroff( winMap, A_REVERSE );;
    wrefresh( winMap );
    getch();

}

void Game::showGame( WINDOW * window ) {

    wattron( window, A_UNDERLINE );
    mvwprintw( window, 1, 1, "Game:" );
    wattroff( window, A_UNDERLINE );
    mvwprintw( window, 3, 1, "Coins: %d", coins );
    mvwprintw( window, 4, 1, "Killed: %d", killedAttackers );

}

void Game::towerAttack() {

    for( unsigned i = 0 ; i < towers.size() ; i++ ){

        vector< pair< int, int > > area = towers[ i ].get()->getAttackingArea();

        for( unsigned j = 0 ; j < area.size() ; j++ ){

            int x = area[ j ].first;
            int y = area[ j ].second;

            int killed = towers[ i ].get()->attack( map.setMap()[ y ][ x ], coins, killedAttackers );

            if ( killed == 1 ){

                for( unsigned k = 0 ; k < attackers.size() ; k++ ){

                    if( attackers[ k ].get()->getPosition().first == x && attackers[ k ].get()->getPosition().second == y ){

                        attackers.erase( attackers.begin() + k );
                        break;

                    }

                }

            }

        }

    }

}

void Game::attackerInteract(){

    for( unsigned i = 0 ; i < attackers.size() ; i++ ){

        int returnValue = attackers[ i ].get()->move( map );

        if( returnValue == 1 ) {

            int x = attackers[ i ].get()->getNextPosition().first;
            int y = attackers[ i ].get()->getNextPosition().second;

            int killed = attackers[ i ].get()->attack( map.setMap()[ y ][ x ], coins, killedAttackers );

            if ( killed == 1 ) {

                for ( unsigned j = 0; j < towers.size(); j++ ) {

                    if ( towers[ j ].get()->getPosition().first == x && towers[ j ].get()->getPosition().second == y ) {

                        towers.erase( towers.begin() + j );
                        break;

                    }

                }

            }
        }
        if( returnValue == 2 ) {

            loss = true;

        }


    }

}

void Game::generateAttacker() {

    int option = rand() % 3;

    if( option == 0 ){

        shared_ptr< Attacker > newAttacker = attackerOne->cloneAttacker();
        newAttacker.get()->setPosition( map.getStart() );

        attackers.push_back( newAttacker );
        countOfAttacker++;


    }
    else if( option == 1 ){

        shared_ptr< Attacker > newAttacker = attackerTwo->cloneAttacker();
        newAttacker.get()->setPosition( map.getStart() );

        attackers.push_back( newAttacker );
        countOfAttacker++;


    }

}

void Game::clearGame() {

    killedAttackers = 0;
    countOfAttacker = 0;
    count = 0;

    towers.clear();
    attackers.clear();
    isRunning = true;
    isPaused = false;
    win = false;
    loss = false;

    delete towerA;
    delete towerB;
    delete towerC;
    delete attackerOne;
    delete attackerTwo;

}

bool Game::saveGame() {

    if( !saveBasics() ){

        return false;

    }

    if( !path.save() ){

        return false;

    }

    if( !saveTowers() ){

        return false;

    }

    if( !saveAttackers() ){

        return false;

    }

   return true;

}

bool Game::saveBasics(){

    ofstream file;
    file.open( "src/files/savedInfoGame.txt", ofstream::trunc );
    if( !file. is_open() ){

        return false;

    };
    if( !file.good() ){

        file.close();
        return false;

    };

    file << coins << "\n"
         << killedAttackers << "\n"
         << countOfAttacker << "\n"
         << chosenMap << endl;

    player.save( file );

    file.close();
    return true;

}

bool Game::saveTowers() {

    ofstream file;
    file.open( "src/files/savedInfoTowers.txt" );
    if( !file. is_open() ){

        return false;

    };
    if( !file.good() ){

        file.close();
        return false;

    };

    file << towers.size() << endl;

    for( unsigned i = 0 ; i < towers.size() ; i++ ){

        towers[ i ].get()->save( file );

    }

    file.close();
    return true;

}

bool Game::saveAttackers() {

    ofstream file;
    file.open( "src/files/savedInfoAttackers.txt" );
    if( !file. is_open() ){

        return false;

    };
    if( !file.good() ){

        file.close();
        return false;

    };

    file << attackers.size() << endl;

    for( unsigned i = 0 ; i < attackers.size() ; i++ ){

        attackers[ i ].get()->save( file );

    }

    file.close();
    return true;


}

void Game::clearFiles() {

    ofstream clearFile;

    clearFile.open( "src/files/savedInfoGame.txt", ofstream::trunc );
    clearFile.close();

    clearFile.open( "src/files/savedInfoTowers.txt", ofstream::trunc );
    clearFile.close();

    clearFile.open( "src/files/savedInfoAttackers.txt", ofstream::trunc );
    clearFile.close();

}

bool Game::loadGame() {

    if( !loadBasics() ){

        return false;

    }

    if( !path.load() ){

        return false;

    }
    attackerOne->setPath( path.getPath() );
    attackerTwo->setPath( path.getPath() );

    if( !loadTowers() ){

        return false;

    }
    if( !loadAttackers() ){

        return false;

    }

    return true;

}

bool Game::loadBasics() {

    ifstream file;
    string readString;

    file.open( "src/files/savedInfoGame.txt" );
    if( !file. is_open() ){

        return false;

    };
    if( !file.good() ){

        file.close();
        return false;

    };

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    coins = stoi( readString );

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    killedAttackers = stoi( readString );

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    countOfAttacker = stoi( readString );

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    int mapNum = stoi( readString );

    if( !map.loadMap( mapNum ) ){

        file.close();
        return false;

    }

    if( !player.load( file ) ){

        file.close();
        return false;

    }

    file.close();
    return true;

}

bool Game::loadTowers() {

    string readString;
    ifstream file;
    file.open("src/files/savedInfoTowers.txt");
    if( !file. is_open() ){

        return false;

    };
    if( !file.good() ){

        file.close();
        return false;

    };

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    int size = stoi( readString );

    for( int i = 0; i < size ; i++ ){

        shared_ptr <Tower> placeTower;
        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        char character = readString[ 0 ];

        if( character == TOWER_A ){

            placeTower = towerA->cloneTower();

        }
        else if( character == TOWER_B ){

            placeTower = towerB->cloneTower();

        }
        else if( character == TOWER_C ){

            placeTower = towerC->cloneTower();

        }

        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        int x = stoi( readString );
        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        int y = stoi( readString );
        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        int life = stoi( readString );

        placeTower.get()->setLife( life );
        placeTower.get()->setAttackingArea( x, y );
        map.setMap()[ y ][ x ] = placeTower;
        towers.push_back( placeTower );

    }
    file.close();
    return true;

}

bool Game::loadAttackers() {

    string readString;
    ifstream file;
    file.open("src/files/savedInfoAttackers.txt");
    if( !file. is_open() ){

        return false;

    };
    if( !file.good() ){

        file.close();
        return false;

    };

    if ( !getline( file, readString ) ){

        file.close();
        return false;

    }
    int size = stoi( readString );

    for( int i = 0; i < size ; i++ ){

        shared_ptr <Attacker> placeAttacker;
        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        char character = readString[ 0 ];

        if( character == ATTACKER_ONE ){

            placeAttacker = attackerOne->cloneAttacker();

        }
        else if( character == ATTACKER_TWO ){

            placeAttacker = attackerTwo->cloneAttacker();

        }

        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        int x = stoi( readString );
        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        int y = stoi( readString );
        if ( !getline( file, readString ) ){

            file.close();
            return false;

        }
        int life = stoi( readString );

        placeAttacker.get()->setLife( life );
        placeAttacker.get()->setPosition( make_pair( x, y ) );
        if( x == map.getStart().first && y == map.getStart().second ){

            break;

        }
        else {

            placeAttacker.get()->loadPositionInPath( x, y );
        }
        map.setMap()[ y ][ x ] = placeAttacker;
        attackers.push_back( placeAttacker );

    }
    shared_ptr< Field > fieldStart = make_shared< Field >( Field() );
    fieldStart.get()->setCharacter( 'S' );
    map.setMap()[ map.getStart().second ][ map.getStart().first ] = fieldStart;

    file.close();
    return true;
}
