#include "attackerOne.hpp"

void AttackerOne::getHurt( int damage, Field & field ){

    field.getPosition();
    life -= damage;

}

int AttackerOne::attack( shared_ptr< Field > & field, int & coins, int & killedAttackers ){

    field->getHurt( attackPower, * this );

    if( field->getLife() <= 0 ){

        killedAttackers -= 1;
        coins -= 5;

        shared_ptr< Field > newField = make_shared< Field >( Field() );
        newField.get()->setCharacter( ' ' );
        field = newField;
        return 1;

    }
    return 0;

}