#pragma once

#include <memory>

#include "tower.hpp"

using namespace std;

/**
 * Represent tower B.
 */

class TowerB : public Tower {

public:

    /**
     * Default constructor.
     */
    TowerB () = default;
    /**
     * Constructor receiving life, characte, attack power and price of tower.
     *
     * @param life life of tower
     * @param character character representing tower
     * @param attackPower attack power of tower
     * @param price price of tower
     */
    TowerB( int life, char character, int attackPower, int price )
            : Tower( life, character, attackPower, price ) {};

    /**
     * Default destructor.
     */
    ~TowerB() override = default;

    /**
     * Make copy of tower.
     *
     * @return new copy of tower
     */
    shared_ptr< Tower > cloneTower () const override { return make_shared< TowerB > ( * this ); };

    /**
     * Take life of field.
     *
     * @param damage power of attack
     * @param field position of attacking field
     */
    void getHurt ( int damage, Field & field ) override;
    /**
     * Attack on the enemy of interacting object and addition/deduction of killed enemies.
     *
     * @param field field on which is attacking
     * @param coins coins of player
     * @param killedAttackers count of killed attackers
     * @return return 1 if object on field is killed, 0 if attack but not kill
     */
    int attack( shared_ptr< Field > & field, int & coins, int & killedAttackers ) override;

};