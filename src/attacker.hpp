#pragma once

#include <memory>
#include <list>

#include "interactObject.hpp"

using namespace std;

/**
 * Represent attacker and his interaction.
 */

class Attacker : public InteractObject {

    public:

        /**
         * Default constructor
         */
        Attacker() = default;
        /**
         * Constructor receiving life, character and attack power of attacker.
         *
         * @param life life of attacker
         * @param character character representing character
         * @param attackPower value of attack power
         */
        Attacker( int life, char character, int attackPower )
                : InteractObject( life, character, attackPower ){};

        /**
         * Make copy of attacker.
         *
         * @return new copy of attacker
         */
        virtual shared_ptr< Attacker > cloneAttacker () const { return make_shared< Attacker >( * this ); };

        /**
         * Moves the attacker position on the map.
         *
         * @param map map of game
         * @return return 0 if attacker moves, 1 if there is tower and 2 if arrived into finish
         */
        int move( Map & map );
        /**
         * Set new path of attacker.
         *
         * @param path path represented by pairs of positions ( x, y )
         */
        void setPath ( list< pair< int, int > > path ){ this->path = path; }
        /**
         * Set position of attacker when game is loaded
         *
         * @param x value of position on the x axis
         * @param y value of position on the y axis
         */
        void loadPositionInPath( int x, int y );
        /**
         * Get next position of attacker.
         *
         * @return position of next field
         */
        pair< int, int > getNextPosition() const { return path.front(); }


    protected:

        /**Path which attacker follows represented by position ( x, y ).*/
        list< pair< int, int > > path;


};