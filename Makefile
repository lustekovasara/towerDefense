
CC=g++

CFLAGS=-std=c++14 -Wall -pedantic -Wno-long-long -O2 -Werror

LIBRARIES=-lncurses

all: compile doc

run: compile
	./lustesar

compile: lustesar

doc: src/main.cpp src/attacker.hpp src/attackerOne.hpp src/attackerTwo.hpp src/field.hpp src/game.hpp src/interactObject.hpp src/map.hpp src/path.hpp src/player.hpp src/tower.hpp src/towerA.hpp src/towerB.hpp src/towerC.hpp
	doxygen Doxyfile

lustesar: src/main.o src/game.o src/field.o src/interactObject.o src/tower.o src/towerA.o src/towerB.o src/towerC.o src/map.o src/attacker.o src/attackerOne.o src/attackerTwo.o src/player.o src/path.o
	$(CC) $(CFLAGS) $^ -o $@ $(LIBRARIES)

src/main.o: src/main.cpp src/game.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/game.o: src/game.cpp src/map.hpp src/tower.hpp src/towerA.hpp src/towerB.hpp src/towerC.hpp src/attacker.hpp src/attackerOne.hpp src/attackerTwo.hpp src/player.hpp src/path.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/field.o: src/field.cpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/interactObject.o: src/interactObject.cpp src/map.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/tower.o: src/tower.cpp src/interactObject.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/towerA.o: src/towerA.cpp src/tower.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/towerB.o: src/towerB.cpp src/tower.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/towerC.o: src/towerC.cpp src/tower.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/map.o: src/map.cpp src/field.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/attacker.o: src/attacker.cpp src/interactObject.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/attackerOne.o: src/attackerOne.cpp src/attacker.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/attackerTwo.o: src/attackerTwo.cpp src/attacker.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/player.o: src/player.cpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

src/path.o: src/path.cpp src/map.hpp
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBRARIES)

clean:
	rm -rf src/*.o doc/ lustesar
